package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/rds/auth"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)


func main() {
	router := mux.NewRouter()
	router.HandleFunc("/users/{userID}", listOfUsers).Methods("POST")
	router.HandleFunc("/users/{userID}/transaction", userTransaction).Methods("POST")
	http.ListenAndServe(":8081", router)
}

type transaction struct {
	Points int `json:"Points"`
	Coupon string `json:"Coupon"`
	Amount int `json:"Amount"`
}


type transactionResponse struct {
	UserId string `json:"UserId"`
	Points int `json:"Points"`
	Amount int `json:"Amount"`
}

type userDetails struct {
	userId string
	points int
}
func listOfUsers(w http.ResponseWriter,  r *http.Request){
	vars := mux.Vars(r)
	userId, ok := vars["userID"]
	if !ok {
		fmt.Println("userID is missing in parameters")
	}

	//err, db := callDb()
	db, err := sql.Open("mysql", "new_user:new_password@tcp(127.0.0.1:3306)/dbs")
	defer db.Close()
	if err != nil {
		fmt.Println("Connection to sql error ", err)
	}

	stmt, err := db.Prepare("INSERT INTO user_app VALUES(?,?)")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("userid --- ", userId)
	_, err = stmt.Exec(userId, 0)
	if err != nil {
		fmt.Println("Error in statament")
		fmt.Println(err)
	}
	w.WriteHeader(http.StatusOK)
}

func userTransaction(w http.ResponseWriter,  r *http.Request){
	vars := mux.Vars(r)
	userId, ok := vars["userID"]
	if !ok {
		fmt.Println("userID is missing in parameters")
	}

	tran := unMarshalTransactionRequestBody(r)

	//err, db := callDb()
	db, err := sql.Open("mysql", "new_user:new_password@tcp(127.0.0.1:3306)/dbs")
	defer db.Close()
	if err != nil {
		fmt.Println("Connection to sql error ", err)
	}

	var u userDetails
	rows, err2 := db.Query("SELECT * from user_app where userId = ?", userId)
	if err2 != nil {
		fmt.Println("Connection to sql error ", err)
	}
	for rows.Next() {
		err = rows.Scan(&u.userId, &u.points)
	}
	var p  int
	var finalAmount int
	if tran.Coupon!="" {
		fmt.Println("p1 is---", p)
		if tran.Coupon == "DISCOUNT10"{
			finalAmount = tran.Amount - tran.Amount*10/100
		}
		if tran.Coupon == "DISCOUNT20"{
			finalAmount = tran.Amount - tran.Amount*20/100
		}
	}

	if u.points==0{
		fmt.Println("p2 is---", p)
		p =  finalAmount/100
	}

	if tran.Points !=0 {
		fmt.Println("p3 is---", p)
		p =  p - tran.Points
		p = p + finalAmount/100
	}


	stmt, err := db.Prepare("INSERT INTO user_app VALUES(?,?)")
	if err != nil {
		fmt.Println(err)
	}
	_, err = stmt.Exec(u.userId, p)
	fmt.Println("p is---", p)
	if err != nil {
		fmt.Println("Error in statament")
		fmt.Println(err)
	}
	fmt.Println(w, "New user transaction compolete")
	userTransactionResponse := transactionResponse{
		UserId: userId,
		Points: p,
		Amount: finalAmount,
	}

	json.NewEncoder(w).Encode(userTransactionResponse)
	w.WriteHeader(http.StatusOK)

}

func unMarshalTransactionRequestBody(request *http.Request) transaction {
	reqBody, _ := ioutil.ReadAll(request.Body)
	var tran transaction
	json.Unmarshal(reqBody, &tran)
	fmt.Println("Transactiopn----", tran)
	return tran
}
func callDb() (error, *sql.DB) {
	dbName := "wallet"
	dbUser := "admin"
	dbHost := "dwallet.cs44sqm0uuz5.ap-southeast-1.rds.amazonaws.com"
	dbPort := 3306
	dbEndpoint := fmt.Sprintf("%s:%d", dbHost, dbPort)
	region := "ap-southeast-1a"

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		fmt.Println("configuration error: " + err.Error())
	}

	authenticationToken, err := auth.BuildAuthToken(
		context.TODO(), dbEndpoint, region, dbUser, cfg.Credentials)
	if err != nil {
		fmt.Println("failed to create authentication token: " + err.Error())
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?tls=true&allowCleartextPasswords=true",
		dbUser, authenticationToken, dbEndpoint, dbName,
	)

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		fmt.Println(err)
	}
	return err, db
}